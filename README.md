<h1 align="center"> 个人博客系统</h1>
<h2 align="center"> 基于Spring+Spring MVC+Mybatis(Maven方式构建)</h2>


### 如何使用
```
$ git clone https://github.com/withstars/Blog-System

$ cd Blog-System

$ mvn clean compile

$ mvn clean package

$ mvn clean install

$ mvn jetty:run

http://localhost:8080
http://localhost:8080/admin 
```



